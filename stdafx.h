// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <assert.h>
#include <stdio.h>
#include <tchar.h>

# define OPENVPN_LOG(args) \
  do { \
      std::ostringstream _ovpn_log; \
      _ovpn_log << args << '\n'; \
  } while (0)

#include "openvpn3/openvpn/io/io.hpp"
#include "openvpn3/openvpn/buffer/buffer.hpp"
#include "openvpn3/openvpn/frame/frame.hpp"
#include "openvpn3/openvpn/frame/frame_init.hpp"

#include "asio/asio/include/asio.hpp"

// TODO: reference additional headers your program requires here
