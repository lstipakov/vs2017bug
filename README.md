# Test case for VS2017 optimization bug #

## Steps to reproduce
1. Open solution in VS2017
2. Set Release/x64 configuration
3. Build project
4. Run project

## Expected results
Program runs and exits.

## Actual results
Program asserts.

## Additional info

Problem only reproduced with VS2017 Release build
when "Whole Program Optimization" is "On".

Problem does not reproduce when "Whole Program Optimization"
is "Off", or on Debug build, or with previous edition of
Visual Studio.

By some reasons (optimization bug?) method
`openvpn::Frame::Context::remaining_payload()` returns `0`.

```
      // How much payload space left in buffer
      size_t remaining_payload(const Buffer& buf) const
      {
        // BUG BUG BUG!!!
        // VS2017 Release with "Whole Program Optimization" ON 
	    // always returns 0, in other cases (optimization off/debug build/VS2015) 
	    // code works as expected

	    if (payload() > buf.size())
	        return payload() - buf.size(); // VS2017 release works fine if "payload()" call is replaced with "payload_"
	    else
	        return 0;
      }
```