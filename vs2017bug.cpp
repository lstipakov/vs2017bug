// vs2017bug.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

using namespace openvpn;

int main()
{
  struct BufferWrapper { BufferAllocated buf; };
  auto fr = frame_init(true, 0, 0, true);
  auto buf = new BufferWrapper();
  (*fr)[Frame::READ_LINK_UDP].prepare(buf->buf);

  asio::io_context ioc;
  asio::ip::udp::socket s(ioc);
  asio::ip::udp::endpoint sender_endpoint;
  auto mb = (*fr)[Frame::READ_LINK_UDP].mutable_buffer(buf->buf);

  assert(mb.size()); // size must be 2048! asserts on VS2017 release build with Whole Program Optimization On

  s.async_receive_from(mb,
    sender_endpoint,
    [buf](const asio::error_code&, const size_t) {}); // this line is required for assert to be fired on previous line
  return 0;
}

